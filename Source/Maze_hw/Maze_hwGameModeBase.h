#pragma once

#include <vector>
#include <stack>
#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "Maze_hwGameModeBase.generated.h"

class ACell;

USTRUCT(BlueprintType)
struct FCell
{
	GENERATED_BODY()

	int32 CoordinatX = 0;
	int32 CoordinatY = 0;
	bool Open_Top = false;
	bool Open_Right = false;
	bool Open_Down = false;
	bool Open_Left = false;

	bool bCellIsBusy = false;
};

USTRUCT(BlueprintType)
struct FBlockingCell
{
	GENERATED_BODY()

	bool Block_Top = false;
	bool Block_Right = false;
	bool Block_Down = false;
	bool Block_Left = false;
};

UCLASS()
class MAZE_HW_API AMaze_hwGameModeBase : public AGameModeBase
{
	GENERATED_BODY()

protected:
	virtual void BeginPlay() override;
	virtual void EndPlay(const EEndPlayReason::Type EndPlayReason) override;

public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Cell")
		TSubclassOf<class AMazeWall> MazeWall;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Cell")
		int32 FieldWidth = 10;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Cell")
		int32 FieldLength = 10;
	
	FVector2D StartCoordinat;
	FVector2D FinishCoordinat;
	
	TArray<FCell> AllCells;

public:
	// основные функции
	UFUNCTION()
		void FillAllCells();
	UFUNCTION()
		void FindPath();
	UFUNCTION()
		void FillAllCells_Maze1();
	UFUNCTION()
		void FillBorder();
	UFUNCTION()
		void FillCurrentCells(const FCell& CurrentCell);

	// вспомогательные функции
	FCell* GetCell(int32 CellCoordinatX, int32 CellCoordinatY);
	void GetNextCell(int32 CurrentCoordinatX, int32 CurrentCoordinatY, std::vector<FCell>* nextStep);

	UFUNCTION()
		void FillWallsForAllCells();
	UFUNCTION()
		void FillOneSide(int32 Fl, int32 Fw);

	// новые функции
	UFUNCTION()
		void DebugInfo(FCell debugCell, FString Comment);
	void StackToArray(std::stack<FCell>* TempStack);

	void GenRandomMaze(float weigh = 0.6f);

	// очень тестовая зона
	void GenMyMaze();
	TArray<FCell*> GetNeighbours(FCell* CurrentCell); // массив соседей
};
