#include "Maze_hwGameModeBase.h"
#include <random>
#include <stack>
#include "MazeWall.h"
#include "Kismet/KismetMathLibrary.h"

void AMaze_hwGameModeBase::BeginPlay()
{
	Super::BeginPlay();

	StartCoordinat.X = 1;
	StartCoordinat.Y = 1;
	FinishCoordinat.X = FieldWidth;
	FinishCoordinat.Y = FieldLength;
	
	FillAllCells(); // заполнить массив пустотой
	FindPath(); // поиск настоящего пути
	//FillAllCells_Maze1(); // тестовые данные
	//GenRandomMaze(); // просто рандомный лабиринт
	//GenMyMaze(); // мой генератор не закончен, НЕ ИСПОЛЬЗОВАТЬ!
	
	FillBorder(); // отрисовка стен
	FillWallsForAllCells(); // отрисовка лабиринта
}

void AMaze_hwGameModeBase::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
	Super::EndPlay(EndPlayReason);
}

// основные функции
void AMaze_hwGameModeBase::FillAllCells()
{
	for (int32 x = 1; x <= FieldWidth; x++)
	{
		for (int32 y = 1; y <= FieldLength; y++)
		{
			FCell NewCell;
			NewCell.CoordinatX = x;
			NewCell.CoordinatY = y;
			NewCell.Open_Top = false;
			NewCell.Open_Right = false;
			NewCell.Open_Down = false;
			NewCell.Open_Left = false;
			NewCell.bCellIsBusy = false;
			AllCells.Add(NewCell);
		}
	}
}

void AMaze_hwGameModeBase::FindPath()
{
	std::stack<FCell> path;
	FCell NewCell;
	NewCell.CoordinatX = StartCoordinat.X;
	NewCell.CoordinatY = StartCoordinat.Y;
	path.push(NewCell);

	int32 CurrentCoordinatX = 0;
	int32 CurrentCoordinatY = 0;
	int32 StepBuild = 0;

	while (!path.empty())
	{
		StepBuild++;
		if (FieldWidth * FieldLength * 2 < StepBuild)
		{
			break;
		}

		FCell LastCell = path.top();
		CurrentCoordinatX = LastCell.CoordinatX;
		CurrentCoordinatY = LastCell.CoordinatY;

		//blocking unnecessary or busy directions
		FBlockingCell _BlockCell;
		int32 NextCoordinatX = 0;
		int32 NextCoordinatY = 0;
		FCell CheckCell;

		_BlockCell.Block_Top = false;
		_BlockCell.Block_Right = false;
		_BlockCell.Block_Down = false;
		_BlockCell.Block_Left = false;
		if (LastCell.CoordinatY == 1)
		{
			_BlockCell.Block_Top = true;
		}
		else if (LastCell.CoordinatY != FieldLength)
		{
			//TOP
			NextCoordinatX = CurrentCoordinatX;
			NextCoordinatY = CurrentCoordinatY;
			--NextCoordinatY;

			CheckCell = *GetCell(NextCoordinatX, NextCoordinatY);
			if (CheckCell.bCellIsBusy)
			{
				_BlockCell.Block_Top = true;
			}

			//DOWN
			NextCoordinatX = CurrentCoordinatX;
			NextCoordinatY = CurrentCoordinatY;
			++NextCoordinatY;

			CheckCell = *GetCell(NextCoordinatX, NextCoordinatY);
			if (CheckCell.bCellIsBusy)
			{
				_BlockCell.Block_Down = true;
			}
		}
		else if (LastCell.CoordinatY == FieldLength)
		{
			_BlockCell.Block_Down = true;
		}

		if (LastCell.CoordinatX == 1)
		{
			_BlockCell.Block_Left = true;
		}
		else if (LastCell.CoordinatX != FieldWidth)
		{
			//RIGHT
			NextCoordinatX = CurrentCoordinatX;
			NextCoordinatY = CurrentCoordinatY;
			++NextCoordinatX;

			CheckCell = *GetCell(NextCoordinatX, NextCoordinatY);
			if (CheckCell.bCellIsBusy)
			{
				_BlockCell.Block_Right = true;
			}

			//LEFT
			NextCoordinatX = CurrentCoordinatX;
			NextCoordinatY = CurrentCoordinatY;
			--NextCoordinatX;

			CheckCell = *GetCell(NextCoordinatX, NextCoordinatY);
			if (CheckCell.bCellIsBusy)
			{
				_BlockCell.Block_Left = true;
			}
		}
		if (LastCell.CoordinatX == FieldWidth)
		{
			_BlockCell.Block_Right = true;
		}

		//Let's try to collect options for the next direction
		std::vector<FCell> NextSteps;

		//TOP
		NextCoordinatX = CurrentCoordinatX;
		NextCoordinatY = CurrentCoordinatY;
		if (!_BlockCell.Block_Top)
		{
			--NextCoordinatY;
			GetNextCell(NextCoordinatX, NextCoordinatY, &NextSteps);
		}

		//RIGHT
		NextCoordinatX = CurrentCoordinatX;
		NextCoordinatY = CurrentCoordinatY;
		if (!_BlockCell.Block_Right)
		{
			++NextCoordinatX;
			GetNextCell(NextCoordinatX, NextCoordinatY, &NextSteps);
		}

		//DOWN
		NextCoordinatX = CurrentCoordinatX;
		NextCoordinatY = CurrentCoordinatY;
		if (!_BlockCell.Block_Down)
		{
			++NextCoordinatY;
			GetNextCell(NextCoordinatX, NextCoordinatY, &NextSteps);
		}

		//LEFT
		NextCoordinatX = CurrentCoordinatX;
		NextCoordinatY = CurrentCoordinatY;
		if (!_BlockCell.Block_Left)
		{
			--NextCoordinatX;
			GetNextCell(NextCoordinatX, NextCoordinatY, &NextSteps);
		}

		//select the next cell, otherwise go back and look for the next cell again
		if (!NextSteps.empty())
		{
			//выбираем сторону из возможных вариантов
			FCell next = NextSteps[rand() % NextSteps.size()];

			FCell CurrentCell;
			CurrentCell.CoordinatX = CurrentCoordinatX;
			CurrentCell.CoordinatY = CurrentCoordinatY;

			FCell NextCell;
			NextCell.CoordinatX = next.CoordinatX;
			NextCell.CoordinatY = next.CoordinatY;

			//Открываем сторону, в которую пошли на ячейках
			if (next.CoordinatX != LastCell.CoordinatX)
			{
				if (LastCell.CoordinatX - next.CoordinatX > 0)
				{
					CurrentCell.Open_Left = true;
					NextCell.Open_Right = true;
				}
				else
				{
					NextCell.Open_Left = true;
					CurrentCell.Open_Right = true;
				}
			}

			if (next.CoordinatY != LastCell.CoordinatY)
			{
				if (LastCell.CoordinatY - next.CoordinatY > 0)
				{
					CurrentCell.Open_Top = true;
					NextCell.Open_Down = true;
				}
				else
				{
					CurrentCell.Open_Down = true;
					NextCell.Open_Top = true;
				}
			}

			FillCurrentCells(CurrentCell);
			//DebugInfo(CurrentCell, "FillCurrentCells");
			FillCurrentCells(NextCell);
			//DebugInfo(NextCell, "FillCurrentCells");

			path.push(next);
		}
		else
		{
			//если пойти никуда нельзя, возвращаемся к предыдущему узлу
			path.pop();
		}
	}
}

void AMaze_hwGameModeBase::FillAllCells_Maze1()
{
	AllCells.Empty();
	FieldWidth = 10;
	FieldLength = 5;
	FinishCoordinat.X = FieldWidth;
	FinishCoordinat.Y = FieldLength;
	
	FCell NewCell11{1, 1, false, false, true, false, true};
	AllCells.Add(NewCell11);
	FCell NewCell12{1, 2, true, false, true, false, true};
	AllCells.Add(NewCell12);
	FCell NewCell13{1, 3, true, true, false, false, true};
	AllCells.Add(NewCell13);
	FCell NewCell14{1, 4, false, true, true, false, true};
	AllCells.Add(NewCell14);
	FCell NewCell15{1, 5, true, true, false, false, true};
	AllCells.Add(NewCell15);

	FCell NewCell21{2, 1, false, true, false, false, true};
	AllCells.Add(NewCell21);
	FCell NewCell22{2, 2, false, true, true, false, true};
	AllCells.Add(NewCell22);
	FCell NewCell23{2, 3, true, false, false, true, true};
	AllCells.Add(NewCell23);
	FCell NewCell24{2, 4, false, true, false, true, true};
	AllCells.Add(NewCell24);
	FCell NewCell25{2, 5, false, false, false, true, true};
	AllCells.Add(NewCell25);

	FCell NewCell31{3, 1, false, true, true, true, true};
	AllCells.Add(NewCell31);
	FCell NewCell32{3, 2, false, false, true, true, true};
	AllCells.Add(NewCell32);
	FCell NewCell33{3, 3, true, true, false, false, true};
	AllCells.Add(NewCell33);
	FCell NewCell34{3, 4, false, false, true, true, true};
	AllCells.Add(NewCell34);
	FCell NewCell35{3, 5, true, true, false, false, true};
	AllCells.Add(NewCell35);

	FCell NewCell41{4, 1, false, true, true, true, true};
	AllCells.Add(NewCell41);
	FCell NewCell42{4, 2, true, false, false, false, true};
	AllCells.Add(NewCell42);
	FCell NewCell43{4, 3, false, false, true, true, true};
	AllCells.Add(NewCell43);
	FCell NewCell44{4, 4, true, false, true, false, true};
	AllCells.Add(NewCell44);
	FCell NewCell45{4, 5, true, true, false, true, true};
	AllCells.Add(NewCell45);

	FCell NewCell51{5, 1, false, true, true, true, true};
	AllCells.Add(NewCell51);
	FCell NewCell52{5, 2, true, true, false, false, true};
	AllCells.Add(NewCell52);
	FCell NewCell53{5, 3, false, true, true, false, true};
	AllCells.Add(NewCell53);
	FCell NewCell54{5, 4, true, false, true, false, true};
	AllCells.Add(NewCell54);
	FCell NewCell55{5, 5, true, false, false, true, true};
	AllCells.Add(NewCell55);

	FCell NewCell61{6, 1, false, true, false, true, true};
	AllCells.Add(NewCell61);
	FCell NewCell62{6, 2, false, true, false, true, true};
	AllCells.Add(NewCell62);
	FCell NewCell63{6, 3, false, true, false, true, true};
	AllCells.Add(NewCell63);
	FCell NewCell64{6, 4, false, true, true, false, true};
	AllCells.Add(NewCell64);
	FCell NewCell65{6, 5, true, true, false, false, true};
	AllCells.Add(NewCell65);

	FCell NewCell71{7, 1, false, true, false, true, true};
	AllCells.Add(NewCell71);
	FCell NewCell72{7, 2, false, false, true, true, true};
	AllCells.Add(NewCell72);
	FCell NewCell73{7, 3, true, false, false, true, true};
	AllCells.Add(NewCell73);
	FCell NewCell74{7, 4, false, true, false, true, true};
	AllCells.Add(NewCell74);
	FCell NewCell75{7, 5, false, true, false, true, true};
	AllCells.Add(NewCell75);

	FCell NewCell81{8, 1, false, false, true, true, true};
	AllCells.Add(NewCell81);
	FCell NewCell82{8, 2, true, true, false, false, true};
	AllCells.Add(NewCell82);
	FCell NewCell83{8, 3, false, false, true, false, true};
	AllCells.Add(NewCell83);
	FCell NewCell84{8, 4, true, true, false, true, true};
	AllCells.Add(NewCell84);
	FCell NewCell85{8, 5, false, true, false, true, true};
	AllCells.Add(NewCell85);

	FCell NewCell91{9, 1, false, true, true, false, true};
	AllCells.Add(NewCell91);
	FCell NewCell92{9, 2, true, true, false, true, true};
	AllCells.Add(NewCell92);
	FCell NewCell93{9, 3, false, true, true, false, true};
	AllCells.Add(NewCell93);
	FCell NewCell94{9, 4, true, true, false, true, true};
	AllCells.Add(NewCell94);
	FCell NewCell95{9, 5, false, false, false, true, true};
	AllCells.Add(NewCell95);

	FCell NewCell101{10, 1, false, false, false, true, true};
	AllCells.Add(NewCell101);
	FCell NewCell102{10, 2, false, false, true, true, true};
	AllCells.Add(NewCell102);
	FCell NewCell103{10, 3, true, false, false, true, true};
	AllCells.Add(NewCell103);
	FCell NewCell104{10, 4, false, false, true, true, true};
	AllCells.Add(NewCell104);
	FCell NewCell105{10, 5, true, false, false, false, true};
	AllCells.Add(NewCell105);
}

void AMaze_hwGameModeBase::FillBorder()
{
	for (int fl = 0; fl < FieldLength; ++fl)
	{
		float InXfl = 0.f;
		float InYfl = 0.f;
		FRotator RotationWallFl = FRotator(0, 0, 0);

		for (int fw = 0; fw < FieldWidth; ++fw)
		{
			if (fw == 0 || fw == FieldWidth - 1)
			{
				InXfl = fl * 180 + 90;
				if (fw == 0)
				{
					InYfl = fw * 180;
				}
				else
				{
					InYfl = fw * 180 + 180;
				}

				const auto World = GetWorld();
				if (World && MazeWall)
				{
					FVector LocationWall = FVector(InXfl, InYfl, 70);
					World->SpawnActor<AMazeWall>(MazeWall, LocationWall, RotationWallFl,
					                                                    FActorSpawnParameters());
				}
				FillOneSide(fl, fw);
			}
			else
			{
				FillOneSide(fl, fw);
			}
		}
	}
}

void AMaze_hwGameModeBase::FillWallsForAllCells()
{
	int32 totalCells = (FieldWidth * FieldLength) - 1;
	for (int elm = 0; elm <= totalCells; elm++)
	{
		auto CurrentCell = AllCells[elm];
		//DebugInfo(CurrentCell, "FillWallsForAllCells");
		int32 InX = CurrentCell.CoordinatX;
		int32 InY = CurrentCell.CoordinatY;
		float InXwall = 0;
		float InYwall = 0;

		//for Horintol==================================
		if (InX != FieldWidth)
		{
			if (!CurrentCell.Open_Right)
			{
				FRotator RotationWallFl = FRotator(0, 0, 0);

				const auto World = GetWorld();
				if (World && MazeWall)
				{
					InXwall = InY * 180 - 90;
					InYwall = InX * 180;
					FVector LocationWall = FVector(InXwall, InYwall, 70);
					AMazeWall* NewBorder = World->SpawnActor<AMazeWall>(MazeWall, LocationWall, RotationWallFl,
					                                                    FActorSpawnParameters());
					NewBorder->MyPosition.Set(float(InX), float(InY)); // записываю внутрь стены инфу
					//UE_LOG(LogTemp, Warning, TEXT("MyPosition: X = %f. Y = %f"), NewBorder->MyPosition.X, NewBorder->MyPosition.Y);
				}
			}
		}

		//for Vertical==================================
		if (InY != FieldLength)
		{
			if (!CurrentCell.Open_Down)
			{
				FRotator RotationWallFl = FRotator(0, 90, 0);

				const auto World = GetWorld();
				if (World && MazeWall)
				{
					InXwall = InY * 180;
					InYwall = InX * 180 - 90;
					FVector LocationWall = FVector(InXwall, InYwall, 70);
					AMazeWall* NewBorder = World->SpawnActor<AMazeWall>(MazeWall, LocationWall, RotationWallFl,
					                                                    FActorSpawnParameters());
					NewBorder->MyPosition.Set(float(InX), float(InY)); // записываю внутрь стены инфу
					//UE_LOG(LogTemp, Warning, TEXT("MyPosition: X = %f. Y = %f"), NewBorder->MyPosition.X, NewBorder->MyPosition.Y);
				}
			}
		}
	}
}

// вспомогательные функции
FCell* AMaze_hwGameModeBase::GetCell(int32 CellCoordinatX, int32 CellCoordinatY)
{
	FCell* FoundCell = nullptr;
	
	int32 totalCells = (FieldWidth * FieldLength) - 1;
	for (int i = 0; i <= totalCells; i++)
	{
		if (AllCells[i].CoordinatX == CellCoordinatX && AllCells[i].CoordinatY == CellCoordinatY)
		{
			FoundCell = &AllCells[i];
			break;
		}
	}

	return FoundCell;
}

void AMaze_hwGameModeBase::GetNextCell(int32 CoordinatX, int32 CoordinatY, std::vector<FCell>* NextSteps)
{
	const FCell CheckNextCell = *GetCell(CoordinatX, CoordinatY);

	if (!CheckNextCell.bCellIsBusy)
	{
		FCell NextCell;
		NextCell.CoordinatX = CoordinatX;
		NextCell.CoordinatY = CoordinatY;
		NextSteps->push_back(NextCell);
	}
}

void AMaze_hwGameModeBase::FillCurrentCells(const FCell& CurrentCell)
{
	FCell* CellForFill = GetCell(CurrentCell.CoordinatX, CurrentCell.CoordinatY);
	CellForFill->Open_Top = CellForFill->Open_Top || CurrentCell.Open_Top;
	CellForFill->Open_Right = CellForFill->Open_Right || CurrentCell.Open_Right;
	CellForFill->Open_Down = CellForFill->Open_Down || CurrentCell.Open_Down;
	CellForFill->Open_Left = CellForFill->Open_Left || CurrentCell.Open_Left;
	CellForFill->bCellIsBusy = true;
}

void AMaze_hwGameModeBase::FillOneSide(int32 Fl, int32 Fw)
{
	float InXfw = 0.f;
	float InYfw = 0.f;
	FRotator RotationWallFl = FRotator(0, 90, 0);

	if (Fl == 0 || Fl == FieldLength - 1)
	{
		if (Fl == 0)
		{
			InXfw = Fl * 180;
		}
		else
		{
			InXfw = Fl * 180 + 180;
		}

		InYfw = Fw * 180 + 90;
		FVector LocationWall = FVector(InXfw, InYfw, 70);

		const auto World = GetWorld();
		if (World && MazeWall)
		{
			World->SpawnActor<AMazeWall>(MazeWall, LocationWall, RotationWallFl,
																FActorSpawnParameters());
		}
	}
}

// новые функции
void AMaze_hwGameModeBase::DebugInfo(FCell debugCell, FString Comment)
{
	UE_LOG(LogTemp, Warning, TEXT("MAZE_DEBUG - %s"), *Comment);
	UE_LOG(LogTemp, Warning, TEXT("MAZE_DEBUG - debugCell X = %d, Y = %d"), debugCell.CoordinatX, debugCell.CoordinatY);
	UE_LOG(LogTemp, Warning, TEXT("MAZE_DEBUG - %s, %s, %s, %s"), debugCell.Open_Top ? TEXT("true") : TEXT("false"),
													debugCell.Open_Right ? TEXT("true") : TEXT("false"),
													debugCell.Open_Down ? TEXT("true") : TEXT("false"),
													debugCell.Open_Left ? TEXT("true") : TEXT("false"));
	UE_LOG(LogTemp, Warning, TEXT("MAZE_DEBUG==============================================="));
}

void AMaze_hwGameModeBase::StackToArray(std::stack<FCell>* TempStack)
{
	AllCells.Empty();
	for (int i = 0; i < FieldWidth*FieldLength; i++)
	{
		AllCells.Add(TempStack->top());
		TempStack->pop();
		UE_LOG(LogTemp, Warning, TEXT("MAZE_DEBUG - i = %d"), i);
	}
}

void AMaze_hwGameModeBase::GenRandomMaze(float weigh)
{
	int32 totalCells = (FieldWidth * FieldLength) - 1;
	for (int elm = 0; elm <= totalCells; elm++)
	{
		FCell* CurrentCell = &AllCells[elm];
		CurrentCell->Open_Top = UKismetMathLibrary::RandomBoolWithWeight(weigh);
		CurrentCell->Open_Right = UKismetMathLibrary::RandomBoolWithWeight(weigh);
		CurrentCell->Open_Down = UKismetMathLibrary::RandomBoolWithWeight(weigh);
		CurrentCell->Open_Left = UKismetMathLibrary::RandomBoolWithWeight(weigh);
	}
}

// очень тестовая зона
TArray<FCell*> AMaze_hwGameModeBase::GetNeighbours(FCell* CurrentCell)
{
	TArray<FCell*> tempNeighbours;
	TArray<FCell*> Neighbours;
	
	int32 curX = CurrentCell->CoordinatX;
	int32 curY = CurrentCell->CoordinatY;

	tempNeighbours[0] = GetCell(curX + 1, curY);
	tempNeighbours[1] = GetCell(curX, curY + 1);
	tempNeighbours[2] = GetCell(curX + 1, curY + 1);
	tempNeighbours[3] = GetCell(curX - 1, curY);
	tempNeighbours[4] = GetCell(curX, curY - 1);
	tempNeighbours[5] = GetCell(curX - 1, curY - 1);
	tempNeighbours[6] = GetCell(curX + 1, curY - 1);
	tempNeighbours[7] = GetCell(curX - 1, curY + 1);

	for (int i = 0; i < 8; i++)
	{
		if (Neighbours[i] && !Neighbours[i]->bCellIsBusy)
		{
			Neighbours.Add(Neighbours[i]);
		}
	}

	return Neighbours;
}

void AMaze_hwGameModeBase::GenMyMaze()
{
	int32 totalCells = (FieldWidth * FieldLength) - 1;
	for (int elm = 0; elm <= totalCells; elm++)
	{
		FCell* CurrentCell = &AllCells[elm];
		if ((CurrentCell->CoordinatX == 5) & (CurrentCell->CoordinatY == 5))
		{
			    FCell* Cell = GetCell(5,5);
				Cell->Open_Right = true;
				Cell->Open_Down = true;
				Cell->Open_Left = true;
				Cell->Open_Top = true;
				//CurrentCell->Open_Down = UKismetMathLibrary::RandomBool();
		}
		else if ((CurrentCell->CoordinatX == FieldWidth-1) & (CurrentCell->CoordinatY == FieldLength-1))
		{
			if (UKismetMathLibrary::RandomBool())
			{
			}
			else
			{
			}
		}
		else
		{

		}
		//DebugInfo(*CurrentCell, "GenMyMaze");
	}
}
